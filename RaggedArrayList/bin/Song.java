import java.util.Comparator;

public class Song implements Comparable<Song> {
	public static final String ARTIST = "ARTIST", TITLE = "TITLE",
			LYRICS = "LYRICS";
	public static int counter = 0;
	private String artist, title, lyrics;

	public Song(String artist, String title, String lyrics) {
		this.artist = artist;
		this.title = title;
		this.lyrics = lyrics;
	}

	public String getArtist() {
		return artist;
	}

	public String getTitle() {
		return title;
	}

	public String getLyrics() {
		return lyrics;
	}

	@Override
	public int compareTo(Song o) {
		counter++;
		Song com = (Song) o;
		int artCom = artist.compareToIgnoreCase(com.getArtist());
		if (artCom == 0) {
			return title.compareToIgnoreCase(com.getTitle());
		}
		return artCom;
	}

	@Override
	public String toString() {
		return ARTIST + ": " + artist + "\n" + TITLE + ": " + title + "\n"
				+ LYRICS + ": " + lyrics;
	}

	public static class PrefixCompare implements Comparator<Song> {

		@Override
		public int compare(Song o1, Song o2) {
			String artist = o1.getArtist().toLowerCase();
			String prefix = o2.getArtist().toLowerCase();
			if (artist.startsWith(prefix)) {
				return 0;
			} else {
				return artist.compareTo(prefix);
			}
		}
	}
}
