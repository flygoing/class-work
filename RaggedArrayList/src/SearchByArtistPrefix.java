import java.util.ArrayList;
import java.util.Arrays;

/*
 * SearchByArtistPrefix.java
 * Starting code Bob Boothe September 2008
 */
public class SearchByArtistPrefix {

	private Song[] songs; // keep a direct reference to the song array

	public SearchByArtistPrefix(SongCollection sc) {
		songs = sc.getAllSongs();
	}

	/**
	 * find all songs matching artist prefix uses binary search should operate
	 * in time log n + k (# matches)
	 * 
	 * @param string
	 * @return
	 */
	public Song[] search(String artistPrefix) {
		Song prefixSong = new Song(artistPrefix, null, null);
		ArrayList<Song> byArtistResult = new ArrayList<Song>();
		Song.PrefixCompare comparator = new Song.PrefixCompare();
		int base = Arrays.binarySearch(songs, prefixSong, comparator);
		if (base < 0) {
			return new Song[0];
		}
		byArtistResult.add(songs[base]);
		for (int count = 1; songs.length > base + count &&comparator.compare(songs[base + count],prefixSong) == 0; count++) {
				byArtistResult.add(songs[base + count]);
		}
		for (int count = 1; base - count >= 0 && comparator.compare(songs[base - count],prefixSong) == 0; count++) {
				byArtistResult.add(0, songs[base - count]);

		}
		return byArtistResult.toArray(new Song[byArtistResult.size()]);
	}

	// testing routine
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("usage: prog songfile artist");
			return;
		}

		SongCollection sc = new SongCollection(args[0]);
		SearchByArtistPrefix sbap = new SearchByArtistPrefix(sc);

		System.out.println("searching for: " + args[1]);
		Song[] byArtistResult = sbap.search(args[1]);

		// to do: show first 10 songs
		for (int count = 0; count < 10 && count < byArtistResult.length; count++) {
			System.out.println(byArtistResult[count]);
		}

		System.err.println("exiting normally");
	}
}
