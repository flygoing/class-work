import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * RaggedArrayList.java
 * PUT YOUR NAME HERE
 * 
 * Initial starting code by Prof. Boothe Sep 2014
 *
 * The RaggedArrayList is a 2 level data structure that is an array of arrays.
 *  
 * It keeps the items in sorted order according to the comparator.
 * Duplicates are allowed.
 * New items are added after any equivalent items.
 */
public class RaggedArrayList<E> implements Iterable<E> {
	private static final int MINIMUM_SIZE = 4; // must be even so when split get
												// two equal pieces
	private int size;
	private Object[] l1Array; // really is an array of L2Array, but compiler
								// won't let me cast to that
	private int l1NumUsed;
	private Comparator<E> comp;

	// create an empty list
	// always have at least 1 second level array even if empty, makes code
	// easier
	// (DONE)
	RaggedArrayList(Comparator<E> c) {
		size = 0;
		l1Array = new Object[MINIMUM_SIZE]; // you can't create an array of a
											// generic type
		l1Array[0] = new L2Array(MINIMUM_SIZE); // first 2nd level array
		l1NumUsed = 1;
		comp = c;
	}

	// nested class for 2nd level arrays
	// (DONE)
	private class L2Array {
		public E[] items;
		public int numUsed;

		L2Array(int capacity) {
			items = (E[]) new Object[capacity]; // you can't create an array of
												// a generic type
			numUsed = 0;
		}
	}

	// total size (number of entries) in the entire data structure
	// (DONE)
	public int size() {
		return size;
	}

	// null out all references so garbage collector can grab them
	// but keep otherwise empty l1Array and 1st L2Array
	// (DONE)
	public void clear() {
		size = 0;
		Arrays.fill(l1Array, 1, l1Array.length, null); // clear all but first l2
														// array
		l1NumUsed = 1;
		L2Array l2Array = (L2Array) l1Array[0];
		Arrays.fill(l2Array.items, 0, l2Array.numUsed, null); // clear out
																// l2array
		l2Array.numUsed = 0;
	}

	// nested class for a list position
	// used only internally
	// 2 parts: level 1 index and level 2 index
	private class ListLoc {
		public int level1Index;
		public int level2Index;

		ListLoc(int level1Index, int level2Index) {
			this.level1Index = level1Index;
			this.level2Index = level2Index;
		}

		// test if two ListLoc's are to the same location
		@Override
		public boolean equals(Object otherObj) {
			if (getClass() != otherObj.getClass()) // not needed since trust
													// will be ListLoc
				return false;
			ListLoc other = (ListLoc) otherObj;

			return level1Index == other.level1Index
					&& level2Index == other.level2Index;
		}

		// move ListLoc to next entry
		// when it moves past the very last entry it will be 1 index past the
		// last value in the used level 2 array
		// used internally to scan through the array for sublist
		// also used to implement the iterator
		public void moveToNext() {
			// NOTE Done I think???
			L2Array ar = (L2Array) l1Array[level1Index];
			if (ar.numUsed - 1 > level2Index) {
				level2Index++;
				return;
			}
			if (l1NumUsed - 1 > level1Index) {
				level1Index++;
				level2Index = 0;
				return;
			}
			if (level1Index != ar.numUsed) {
				if (l1NumUsed > level1Index) {
					level1Index++;
				}
			}
		}
	}

	/**
	 * find 1st matching entry returns ListLoc of 1st matching item or of 1st
	 * item greater than the item if no match, this could be a used slot at the
	 * end of a level 2 array searches forwards
	 */
	private ListLoc findFront(E item) {
		// NOTE Done I think???
		ListLoc loc = new ListLoc(0, 0);
		L2Array curAr = null;
		for (loc.level1Index = 0; loc.level1Index < l1NumUsed; loc.level1Index++) {
			curAr = (L2Array) l1Array[loc.level1Index];
			loc.level2Index = curAr.numUsed - 1;
			for (loc.level2Index = 0; loc.level2Index < curAr.numUsed; loc.level2Index++) {
				int com = comp.compare(item, curAr.items[loc.level2Index]);
				if (com <= 0) {
					return loc;
				}
			}
		}
		loc.level2Index++;
		return loc;
	}

	/**
	 * find location after the last matching entry or if no match, it finds the
	 * index of the next larger item this is the position to add a new entry
	 * this could be an unused slot at the end of a level 2 array
	 **/
	private ListLoc findEnd(E item) {
		// NOTE Done I think???
		L2Array curAr = ((L2Array) l1Array[l1NumUsed - 1]);
		ListLoc loc = new ListLoc(l1NumUsed - 1, curAr.numUsed - 1);
		if (loc.level2Index < 0) {
			loc.level2Index = 0;
		}
		if (((L2Array) l1Array[loc.level1Index]).numUsed == 0) {
			return loc;
		}
		for (int level1 = l1NumUsed - 1; level1 >= 0; level1--) {
			curAr = (L2Array) l1Array[level1];
			for (int level2 = curAr.numUsed - 1; level2 >= 0; level2--) {
				int com = comp.compare(item, curAr.items[level2]);
				if (com >= 0) {
					loc.level1Index = level1;
					loc.level2Index = level2 + 1;
					return loc;
				}
			}
		}
		loc.level1Index = 0;
		loc.level2Index = 0;
		return loc;
	}

	/**
	 * add object after any other matching values findEnd will give the
	 * insertion position
	 */
	boolean add(E item) {
		// NOTE Done I think???
		size++;
		ListLoc loc = findEnd(item);
		L2Array ar = (L2Array) l1Array[loc.level1Index];
		if (ar.items[loc.level2Index] == null) {
			ar.items[loc.level2Index] = item;
			ar.numUsed++;
		} else {
			for (int count = ar.items.length - 1; count > loc.level2Index; count--) {
				ar.items[count] = ar.items[count - 1];
			}
			ar.items[loc.level2Index] = item;
			ar.numUsed++;
		}
		if (ar.numUsed == ar.items.length) {
			if (ar.numUsed == l1Array.length) {
				L2Array newFirst = new L2Array(ar.numUsed);
				L2Array newSecond = new L2Array(ar.numUsed);
				for (int count = 0; count < ar.numUsed; count++) {
					if (count < ar.numUsed / 2) {
						newFirst.items[count] = ar.items[count];
					} else {
						newSecond.items[count - ar.numUsed / 2] = ar.items[count];
					}
				}
				newFirst.numUsed = ar.numUsed / 2;
				newSecond.numUsed = newFirst.numUsed;
				l1Array[loc.level1Index] = newFirst;
				for (int count = l1Array.length - 1; count > loc.level1Index + 1; count--) {
					l1Array[count] = l1Array[count - 1];
				}
				l1Array[loc.level1Index + 1] = newSecond;
				l1NumUsed++;
			} else {
				L2Array newArray = new L2Array(l1Array.length);
				for (int count = 0; count < ar.items.length; count++) {
					newArray.items[count] = ar.items[count];
				}
				newArray.numUsed = ar.numUsed;
				l1Array[loc.level1Index] = newArray;
			}
		}
		if (l1NumUsed == l1Array.length) {
			Object[] newL1Array = new Object[l1Array.length * 2];
			for (int count = 0; count < l1NumUsed; count++) {
				newL1Array[count] = l1Array[count];
			}
			l1Array = newL1Array;

		}
		System.out.println(((CmpCnt)comp).getCmpCnt());
		return true;
	}

	/**
	 * check if list contains a match
	 */
	boolean contains(E item) {
		// NOTE Done I think???
		ListLoc loc = new ListLoc(0, 0);
		L2Array curAr = null;
		for (loc.level1Index = 0; loc.level1Index < l1NumUsed; loc.level1Index++) {
			curAr = (L2Array) l1Array[loc.level1Index];
			for (loc.level2Index = 0; loc.level2Index < curAr.numUsed; loc.level2Index++) {
				int com = comp.compare(item, curAr.items[loc.level2Index]);
				if (com == 0) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * copy the contents of the RaggedArrayList into the given array
	 * 
	 * @param a
	 *            - an array of the actual type and of the correct size
	 * @return the filled in array
	 */
	public E[] toArray(E[] a) {
		// NOTE Done I think???
		int count = 0;
		Iterator<E> itr = iterator();
		while (itr.hasNext()) {
			a[count] = itr.next();
			count++;
		}

		return a;
	}

	/**
	 * returns a new independent RaggedArrayList whose elements range from
	 * fromElemnt, inclusive, to toElement, exclusive the original list is
	 * unaffected
	 * 
	 * @param fromElement
	 * @param toElement
	 * @return the sublist
	 */
	public RaggedArrayList<E> subList(E fromElement, E toElement) {
		// NOTE Done I think???
		Iterator itr = iterator();
		RaggedArrayList<E> result = new RaggedArrayList<E>(comp);
		boolean started = false;
		while (itr.hasNext()) {
			E e = (E) itr.next();
			if (comp.compare(e, toElement) >= 0) {
				break;
			}
			if (comp.compare(e, fromElement) == 0) {
				started = true;
			}
			if (started) {
				result.add(e);
			}
		}
		return result;
	}

	/**
	 * returns an iterator for this lismt this method just creates an instance
	 * of the inner Itr() class (DONE)
	 */
	public Iterator<E> iterator() {
		return new Itr();
	}

	/**
	 * Iterator is just a list loc it starts at (0,0) and finishes with index2 1
	 * past the last item in the last block
	 */
	private class Itr implements Iterator<E> {
		private ListLoc loc;

		/*
		 * create iterator at start of list (DONE)
		 */
		Itr() {
			loc = new ListLoc(0, 0);
		}

		/**
		 * check if more items
		 */
		public boolean hasNext() {
			// NOTE Done I think???
			L2Array ar = (L2Array) l1Array[loc.level1Index];
			if (l1NumUsed  > loc.level1Index) {

				if (ar.numUsed > loc.level2Index) {
					return true;
				}
			}
			return false;
		}

		/**
		 * return item and move to next throws NoSuchElementException if off end
		 * of list
		 */
		public E next() {
			// NOTE Done I think???`
			if (l1NumUsed <= loc.level1Index) {
				throw new NoSuchElementException();
			}
			E el = ((L2Array) l1Array[loc.level1Index]).items[loc.level2Index];
			loc.moveToNext();
			return el;
		}

		/**
		 * Remove is not implemented. Just use this code. (DONE)
		 */
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	/**
	 * Main routine for testing the RaggedArrayList by itself. There is a
	 * default test case of a-g. You can also specify arguments on the command
	 * line that will be processed as a sequence of characters to insert into
	 * the list.
	 * 
	 * DO NOT MODIFY I WILL BE USING THIS FOR MY TESTING
	 * 
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException {
		System.out.println("testing routine for RaggedArrayList");
		System.out
				.println("usage: any command line arguments are added by character to the list");
		System.out
				.println("       if no arguments, then a default test case is used");

		// setup the input string
		String order = "";
		if (args.length == 0)
			order = "abcdefg"; // default test
		else
			for (int i = 0; i < args.length; i++)
				// concatenate all args
				order += args[i];

		// insert them character by character into the list
		System.out.println("insertion order: " + order);
		Comparator<String> comp = new StringCmp();
		((CmpCnt) comp).resetCmpCnt(); // reset the counter inside the
										// comparator
		RaggedArrayList<String> ralist = new RaggedArrayList<String>(comp);
		for (int i = 0; i < order.length(); i++) {
			String s = order.substring(i, i + 1);
			ralist.add(s);
		}
		System.out
				.println("The number of comparison to build the RaggedArrayList = "
						+ ((CmpCnt) comp).getCmpCnt());

		System.out.println("TEST: after adds - data structure dump");
		ralist.dump();
		ralist.stats();

		System.out.println("TEST: contains(\"c\") ->" + ralist.contains("c"));
		System.out.println("TEST: contains(\"7\") ->" + ralist.contains("7"));

		System.out.println("TEST: toArray");
		String[] a = new String[ralist.size()];
		ralist.toArray(a);
		for (int i = 0; i < a.length; i++)
			System.out.print("[" + a[i] + "]");
		System.out.println();

		System.out.println("TEST: iterator");
		Iterator<String> itr = ralist.iterator();
		while (itr.hasNext())
			System.out.print("[" + itr.next() + "]");
		System.out.println();

		System.out.println("TEST: sublist(b,k)");
		RaggedArrayList<String> sublist = ralist.subList("b", "k");
		sublist.dump();
	}

	/**
	 * string comparator with cmpCnt for testing
	 * 
	 * DO NOT MODIFY I WILL BE USING THIS FOR MY TESTING
	 */
	public static class StringCmp implements Comparator<String>, CmpCnt {
		int cmpCnt;

		StringCmp() {
			cmpCnt = 0;
		}

		public int getCmpCnt() {
			return cmpCnt;
		}

		public void resetCmpCnt() {
			this.cmpCnt = 0;
		}

		public int compare(String s1, String s2) {
			cmpCnt++;
			return s1.compareTo(s2);
		}
	}

	/**
	 * print out an organized display of the list intended for testing purposes
	 * on small examples it looks nice for the test case where the objects are
	 * characters
	 * 
	 * DO NOT MODIFY I WILL BE USING THIS FOR MY TESTING
	 */
	public void dump() {
		System.out.println("DUMP: Display of the raggedArrayList");
		for (int i1 = 0; i1 < l1Array.length; i1++) {
			L2Array l2array = (L2Array) l1Array[i1];
			System.out.print("[" + i1 + "] -> ");
			if (l2array == null)
				System.out.println("null");
			else {
				for (int i2 = 0; i2 < l2array.items.length; i2++) {
					E item = l2array.items[i2];
					if (item == null)
						System.out.print("[ ]");
					else
						System.out.print("[" + item + "]");
				}
				System.out.println("  (" + l2array.numUsed + " of "
						+ l2array.items.length + ") used");
			}
		}
	}

	/**
	 * calculate and display statistics
	 * 
	 * It use a comparator that implements the given CmpCnt interface. It then
	 * runs through the list searching for every item and calculating search
	 * statistics.
	 * 
	 * DO NOT MODIFY I WILL BE USING THIS FOR MY TESTING
	 */
	public void stats() {
		System.out.println("STATS:");
		System.out.println("list size N = " + size);

		// level 1 array
		System.out.println("level 1 array " + l1NumUsed + " of "
				+ l1Array.length + " used.");

		// level 2 arrays
		int minL2size = Integer.MAX_VALUE, maxL2size = 0;
		for (int i1 = 0; i1 < l1NumUsed; i1++) {
			L2Array l2Array = (L2Array) l1Array[i1];
			minL2size = Math.min(minL2size, l2Array.numUsed);
			maxL2size = Math.max(maxL2size, l2Array.numUsed);
		}
		System.out.println("level 2 array sizes: min = " + minL2size
				+ " used, avg = " + (double) size / l1NumUsed + " used, max = "
				+ maxL2size + " used");

		// search stats, search for every item
		int totalCmps = 0, minCmps = Integer.MAX_VALUE, maxCmps = 0;
		Iterator<E> itr = iterator();
		while (itr.hasNext()) {
			E obj = itr.next();
			System.out.println(obj);
			((CmpCnt) comp).resetCmpCnt();
			if (!contains(obj))
				System.err
						.println("Did not expect an unsuccesful search in stats");
			int cnt = ((CmpCnt) comp).getCmpCnt();
			totalCmps += cnt;
			if (cnt > maxCmps)
				maxCmps = cnt;
			if (cnt < minCmps)
				minCmps = cnt;
		}
		System.out.println("Successful search: min cmps = " + minCmps
				+ " avg cmps = " + (double) totalCmps / size + " max cmps = "
				+ maxCmps);
	}
}