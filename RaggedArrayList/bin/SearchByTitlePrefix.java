import java.util.Comparator;
import java.util.Iterator;

public class SearchByTitlePrefix {
	private RaggedArrayList<Song> list;

	public SearchByTitlePrefix(SongCollection songs) {
		list = new RaggedArrayList<Song>(new TitleComparator());
		Song[] songArray = songs.getAllSongs();
		for (int count = 0; count < songArray.length; count++) {
			list.add(songArray[count]);
		}
	}

	public Song[] search(String prefix) {
		String fixed = prefix.substring(0, prefix.length() - 1);
		fixed += (char)(prefix.charAt(prefix.length()-1)+1);
		RaggedArrayList<Song> newList = list.subList(new Song(null, prefix,
				null), new Song(null, fixed, null));
		Song[] sublist = new Song[newList.size()];
		sublist = newList.toArray(sublist);
		return sublist;
	}

	private class TitleComparator implements Comparator<Song>, CmpCnt {
		private int count = 0;

		@Override
		public int getCmpCnt() {
			// TODO Auto-generated method stub
			return count;
		}

		@Override
		public void resetCmpCnt() {
			// TODO Auto-generated method stub
			count = 0;
		}

		@Override
		public int compare(Song arg0, Song arg1) {
			count++;
			String lowerFirst = arg0.getTitle().toLowerCase();
			String lowerSecond = arg1.getTitle().toLowerCase();
			if (lowerFirst.startsWith(lowerSecond)) {
				return 0;
			}
			return lowerFirst.compareTo(lowerSecond);
		}

	}
}
