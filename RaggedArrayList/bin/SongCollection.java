import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * SongCollection.java
 * Read the specified data file and build an array of songs.
 * 
 * Initial code by Bob Boothe September 2008
 */
public class SongCollection {

	private Song[] songs; // array of all the songs

	// read in the song file and build the songs array
	public SongCollection(String filename) {
		// read in the song file and build the songs array - DONE
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<Song> songList = new ArrayList<Song>();
		String line = "";
		String artist = null;
		String title = null;
		String lyrics = null;
		try {
			while (true) {
				if ((line = br.readLine()) == null) {
					break;
				}
				artist = line.split("=")[1].replaceAll("\"", "").trim();
				line=br.readLine();
				title = line.split("=")[1].replaceAll("\"", "").trim();
				line=br.readLine();
				while (!line.endsWith("\"")) {
					line+="\n";
					line += br.readLine();
				}
				lyrics = line.split("=")[1].replaceAll("\"", "").trim();
				songList.add(new Song(artist, title, lyrics));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// sort the songs array - DONE
		songs = new Song[songList.size()];
		songs = songList.toArray(songs);
		Arrays.sort(songs);
		// print statistics:
		// the number of songs - DONE
		System.out.println("There are - songs".replaceFirst("-",
				String.valueOf(songs.length)));
		// the number of comparisons used to sort it - DONE
		System.out.println("There were - comparisons".replaceFirst("-",
				String.valueOf(Song.counter)));
	}

	// return the songs array
	// this is used as the data source for building other data structures
	public Song[] getAllSongs() {
		return songs;
	}

	// testing method
	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("usage: prog songfile");
			return;
		}

		SongCollection sc = new SongCollection(args[0]);
		Song[] songs = sc.getAllSongs();
		// todo: show First 10 songs
		for (int count = 0; count < 10 && count<songs.length; count++) {
			System.out.println(songs[count]);
		}
	}

}
